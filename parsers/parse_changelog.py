"""
parse_changelog.py
Parse Slackware (and friends) ChangeLog.txt.
Requires python3, and ply (built with python3 support).
dbs 2018-09-08 Unlicense http://unlicense.org/

Provides:

    parse_changelog(clpath)
        (returns parsed contents of 'clpath' as a list of dicts)

"""

#-------------------------------------------------------------------------------
#
#   Implementation Note
#
#   We generate a list of dicts to hold the parsed changelog.
#
#   Each dict in the list represents a changelog entry, as follows:
#       "date"
#           string (in the changelog's original format),
#       "motd"
#           the entry's optional message of the day, or a null string,
#       "itemlist"
#           a list of dicts, as follows:
#           {   "itemname"
#                   often a package name, but also 'isolinux/initrd.img' etc.
#               "description"
#                   typically something like "Added...", "Updated..." etc,
#               "securityfix"
#                   True or False
#           }
#
#   Whitespace and newlines are preserved in "motd" and "description",
#   except that the description's required one- or two-space indentation is
#   removed.
#
#-------------------------------------------------------------------------------

import sys
import logging
from ply import *

#-------------------------------------------------------------------------------

states = (
    ('header',   'exclusive'),
    ('itemdesc', 'exclusive')
    )

tokens = (
    'SEPARATOR',
    'BLANK',
    'DATE',
    'MOTDCHUNK',
    'ITEMNAME',
    'DESCRIPTIONCHUNK',
    'SECURITYFIX'
    )

# INITIAL state

def t_INITIAL_SEPARATOR(t):
    r'\+--------------------------\+\n+'
    t.lexer.lineno += 1
    pass

def t_INITIAL_BLANK(t):
    r'\ *\n'
    t.lexer.lineno += 1
    pass

def t_INITIAL_DATE(t):
    r'.*\n'
    t.lexer.lineno += 1
    t.value = t.value.rstrip()
    t.lexer.begin('header')
    return t

# header state

def t_header_ITEMNAME(t):
    r'[^ :]+: +'
    t.value = t.value.rstrip(": ")
    t.lexer.begin('itemdesc')
    return t

def t_header_SEPARATOR(t):
    r'\+--------------------------\+\n+'
    t.lexer.lineno += 1
    t.lexer.begin('INITIAL')
    pass

def t_header_MOTDCHUNK(t):
    r'.*\n'
    t.lexer.lineno += 1
    return t

# itemdesc state

def t_itemdesc_SECURITYFIX(t):
    r'[(* ]+[Ss]ecurity\ [Ff]ix[)* ]+\n'
    t.lexer.lineno += 1
    return t

def t_itemdesc_SEPARATOR(t):
    r'\+--------------------------\+\n+'
    t.lexer.lineno += 1
    t.lexer.begin('INITIAL')
    pass

def t_itemdesc_ITEMNAME(t):
    r'[^ :]+: +'
    t.value = t.value.rstrip(": ")
    t.lexer.begin('itemdesc')
    return t

def t_itemdesc_DESCRIPTIONCHUNK(t):
    r'.*\n'
    if t.value.startswith("  "):
        t.value = t.value[2:]
    elif t.value.startswith(" "):
        t.value = t.value[1:]
    t.lexer.lineno += 1
    return t

# others

def t_ANY_error(t):
    logging.error("We are lost at '%s'" % t.value[0])
    t.lexer.skip(1)

def t_ANY_eof(t):
    return None

#-------------------------------------------------------------------------------

lexer = lex.lex()

#-------------------------------------------------------------------------------


def p_entrylist(p):
    """
    entrylist : entry entrylist
              | entry
    """
    if len(p) == 3:
        # append an entry to an existing entrylist
        p[0] = p[2]
        p[0].append(p[1])
    elif len(p) == 2:
        # create a new entrylist instance
        p[0] = [p[1]]


def p_entry(p):
    """
    entry : header itemlist
          | header
    """
    # create a new entry instance
    if len(p) == 3:
        # copy the header (which is a dict) with a new key for the itemlist
        p[0] = p[1]
        p[0]["itemlist"] = p[2]
    elif len(p) == 2:
        # just copy the header, with an empty itemlist
        p[0] = p[1]
        p[0]["itemlist"] = []


def p_header(p):
    """
    header : DATE motd
           | DATE
    """
    # create a new header instance
    if len(p) == 3:
        p[0] = { "date": p[1], "motd": p[2] }
    elif len(p) == 2:
        p[0] = { "date": p[1], "motd": ""   }


def p_motd(p):
    """
    motd : MOTDCHUNK motd
         | MOTDCHUNK
    """
    if len(p) == 3:
        # concatenate sequences of MOTDCHUNK
        p[0] = p[1] + p[2]
    elif len(p) == 2:
        p[0] = p[1]


def p_itemlist(p):
    """
    itemlist : item itemlist
             | item
    """
    if len(p) == 3:
        # append an item to existing list
        p[0] = p[2]
        p[0].append(p[1])
    elif len(p) == 2:
        # create a new list
        p[0] = [p[1]]


def p_item(p):
    """
    item : ITEMNAME descseq SECURITYFIX descseq
         | ITEMNAME descseq SECURITYFIX
         | ITEMNAME descseq
    """
    description = p[2]
    securityfix = False
    if len(p) == 5:
        description = description + p[4]
        securityfix = True
    elif len(p) == 4:
        securityfix = True
    p[0] = {"itemname":p[1], "description":description, "securityfix":securityfix}


def p_descseq(p):
    """
    descseq : DESCRIPTIONCHUNK descseq
            | DESCRIPTIONCHUNK
    """
    if len(p) == 3:
        # concatenate description fragments
        p[0] = p[1] + p[2]
    elif len(p) == 2:
        p[0] = p[1]


def p_error(p):
    logging.error("Syntax error in input!")

#-------------------------------------------------------------------------------

parser = yacc.yacc()

#-------------------------------------------------------------------------------

def parse_changelog(clpath):
    """
    Function to parse the contents of a ChangeLog.txt file.
    Argument:
        clpath -- the path to the file
    Returns:  a list of dicts, as described in the "Implementation Note".
    """
    with open(clpath) as changelogtxt:
        cldata = parser.parse(changelogtxt.read())
    return cldata
