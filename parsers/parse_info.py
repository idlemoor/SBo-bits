"""
parse_info.py
Parse a SlackBuilds.org info file.
Requires python3, and ply (built with python3 support).
dbs 2018-09-08 Unlicense http://unlicense.org/

Provides:

    parse_info(infopath)
        (returns parsed contents of 'infopath' as a dict)

"""

#-------------------------------------------------------------------------------
#
#   Implementation Note
#
#   We generate a dict of the .info file's keys and values.
#
#   The format of the dict is identical to the .info file's keys and
#   values, except that DOWNLOAD*, MD5SUM* and REQUIRES are always lists.
#     {
#       PRGNAM:   "",
#       VERSION:  "",
#       HOMEPAGE: "",
#       DOWNLOAD: [ "url"... ],
#       MD5SUM:   [ "md5sum"... ],
#       DOWNLOAD_x86_64: [ "url"... ],
#       MD5SUM_x86_64:   [ "md5sum"... ],
#       REQUIRES: [ "dep"... ]
#       MAINTAINER: "",
#       EMAIL:    ""
#     }
#
#-------------------------------------------------------------------------------

import sys
import logging
from ply import *

#-------------------------------------------------------------------------------

states = (
    ('startmulti', 'exclusive'),
    ('multi',      'exclusive')
    )

tokens = (
    "PRGNAM",
    "VERSION",
    "HOMEPAGE",
    "DOWNLOAD",
    "MD5SUM",
    "DOWNLOAD_x86_64",
    "MD5SUM_x86_64",
    "REQUIRES",
    "MAINTAINER",
    "EMAIL",
    "EQUALS",
    "NEWLINE",
    "BACKSLASH",
    "DQUOTE",
    "BLANKSEQ",
    "MD5VALUE",
    "STRING",
    )

# The order of these token regexes is *very* important, which is why they are
# defined as functions instead of strings (see the ply documentation).

# INITIAL state

def t_INITIAL_PRGNAM(t):
    r"PRGNAM"
    return t

def t_INITIAL_VERSION(t):
    r"VERSION"
    return t

def t_INITIAL_HOMEPAGE(t):
    r"HOMEPAGE"
    return t

def t_INITIAL_DOWNLOAD_x86_64(t):
    # this must precede t_DOWNLOAD
    r"DOWNLOAD_x86_64"
    t.lexer.begin('startmulti')
    return t

def t_INITIAL_DOWNLOAD(t):
    r"DOWNLOAD"
    t.lexer.begin('startmulti')
    return t

def t_INITIAL_MD5SUM_x86_64(t):
    # this must precede t_MD5SUM
    r"MD5SUM_x86_64"
    t.lexer.begin('startmulti')
    return t

def t_INITIAL_MD5SUM(t):
    r"MD5SUM"
    t.lexer.begin('startmulti')
    return t

def t_INITIAL_REQUIRES(t):
    r"REQUIRES"
    t.lexer.begin('startmulti')
    return t

def t_INITIAL_MAINTAINER(t):
    r"MAINTAINER"
    return t

def t_INITIAL_EMAIL(t):
    r"EMAIL"
    return t

def t_INITIAL_EQUALS(t):
    r"="
    return t

def t_INITIAL_DQUOTE(t):
    # don't support single-quoted strings
    r'"'
    return t

def t_INITIAL_NEWLINE(t):
    r"\n"
    t.lexer.lineno += 1
    return t

def t_INITIAL_STRING(t):
    # this can contain whitespace but doesn't support escaped quotes
    r'[^"]+'
    return t

# startmulti state (this is the context for EQUALS DQUOTE preceding
# 'multistring' or 'multimd5value' -- see the parser below)

def t_startmulti_EQUALS(t):
    r"="
    return t

def t_startmulti_DQUOTE(t):
    # this is always an opening quote and it moves us onto 'multi' state
    r'"'
    t.lexer.begin('multi')
    return t

# multi state (this is the context for whitespace-separated multiple strings
# or md5 values)

def t_multi_BLANKSEQ(t):
    # spaces and tabs only -- CR FF VT and Unicode whitespace are not supported
    r"[ \t]+"
    return t

def t_multi_BACKSLASH(t):
    r"\\"
    return t

def t_multi_NEWLINE(t):
    r"\n"
    t.lexer.lineno += 1
    return t

def t_multi_DQUOTE(t):
    # this is always a closing quote, and it returns us to 'INITIAL' state
    r'"'
    t.lexer.begin('INITIAL')
    return t

def t_multi_MD5VALUE(t):
    # case insensitive and exactly 32 chars long
    # this must precede t_multi_STRING
    r"[0-9a-zA-Z]{32}"
    t.value = t.value.lower()
    return t

def t_multi_STRING(t):
    # this can't contain whitespace and doesn't support escaped quotes
    r'[^" \t]+'
    return t

# ANY state -- error and eof handling

def t_ANY_error(t):
    print("We are lost at '%s'" % t.value[0])
    # logging.error("We are lost at '%s'" % t.value[0])
    t.lexer.skip(1)
    return t

def t_ANY_eof(t):
    return None

#-------------------------------------------------------------------------------

lexer = lex.lex()

#-------------------------------------------------------------------------------

def p_infofile(p):
    """
    infofile : prgnam version homepage download md5sum download_64 md5sum_64 requires maintainer email
    """
    p[0] = {  "PRGNAM":     p[1],
              "VERSION":    p[2],
              "HOMEPAGE":   p[3],
              "DOWNLOAD":   p[4],
              "MD5SUM":     p[5],
              "DOWNLOAD_x86_64": p[6],
              "MD5SUM_x86_64":   p[7],
              "REQUIRES":   p[8],
              "MAINTAINER": p[9],
              "EMAIL":      p[10]
              }

def p_prgnam(p):
    """
    prgnam : PRGNAM EQUALS DQUOTE STRING DQUOTE NEWLINE
    """
    p[0] = p[4]

def p_version(p):
    """
    version : VERSION EQUALS DQUOTE STRING DQUOTE NEWLINE
    """
    p[0] = p[4]

def p_homepage(p):
    """
    homepage : HOMEPAGE EQUALS DQUOTE STRING DQUOTE NEWLINE
    """
    p[0] = p[4]

def p_download(p):
    """
    download : DOWNLOAD EQUALS multistring NEWLINE
    """
    p[0] = p[3]

def p_download_64(p):
    """
    download_64 : DOWNLOAD_x86_64 EQUALS multistring NEWLINE
    """
    p[0] = p[3]

def p_md5sum(p):
    """
    md5sum : MD5SUM EQUALS multimd5value NEWLINE
    """
    p[0] = p[3]

def p_md5sum_64(p):
    """
    md5sum_64 : MD5SUM_x86_64 EQUALS multimd5value NEWLINE
    """
    p[0] = p[3]

def p_requires(p):
    """
    requires : REQUIRES EQUALS multistring NEWLINE
    """
    p[0] = p[3]

def p_maintainer(p):
    """
    maintainer : MAINTAINER EQUALS DQUOTE STRING DQUOTE NEWLINE
    """
    p[0] = p[4]

def p_email(p):
    """
    email : EMAIL EQUALS DQUOTE STRING DQUOTE NEWLINE
    """
    p[0] = p[4]

def p_multistring(p):
    """
    multistring : DQUOTE strings DQUOTE
                | DQUOTE DQUOTE
    """
    if len(p) == 4:
        p[0] = p[2]
    else:
        p[0] = []

def p_strings(p):
    """
    strings  : strings whitespace STRING
             | STRING
    """
    if len(p) == 4:
        p[0] = p[1]
        p[0].append(p[3])
    else:
        p[0] = [ p[1] ]

def p_multimd5value(p):
    """
    multimd5value : DQUOTE md5values DQUOTE
                  | DQUOTE DQUOTE
    """
    if len(p) == 4:
        p[0] = p[2]
    else:
        p[0] = []

def p_md5values(p):
    """
    md5values  : md5values whitespace MD5VALUE
               | MD5VALUE
    """
    if len(p) == 4:
        p[0] = p[1]
        p[0].append(p[3])
    else:
        p[0] = [ p[1] ]

def p_whitespace(p):
    """
    whitespace : BLANKSEQ BACKSLASH NEWLINE BLANKSEQ
               | BLANKSEQ BACKSLASH NEWLINE
               | BLANKSEQ
    """
    p[0] = None

def p_error(p):
    # logging.error("Syntax error in input!")
    print("Syntax error in input!")

#-------------------------------------------------------------------------------

parser = yacc.yacc()

#-------------------------------------------------------------------------------

def parse_info(infopath):
    """
    Function to parse the contents of a SlackBuilds.org info file.
    Argument:
        infopath -- the path to the file
    Returns:  a dict, as described in the "Implementation Note".
    """

    try:
        with open(infofile, "r", encoding="utf-8") as infofile:
            Infodict = parser.parse(infofile.read())
    except:
        with open(infofile, "r", encoding="latin-1") as infofile:
            Infodict = parser.parse(infofile.read())

    return Infodict

#-------------------------------------------------------------------------------
