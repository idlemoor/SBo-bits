"""
parse_pkgpath.py
Parse a Slackware package name.
Requires python3.
dbs 2018-09-08 Unlicense http://unlicense.org/

Provides:

    parse_pkgpath(pkgpath)
        (returns parsed pathname 'pkgpath' as a tuple of 7 strings)

"""

import re
import sys

#-------------------------------------------------------------------------------

re_build  = re.compile(r"[0-9]+")
re_pkgext = re.compile(r"\.t.z.*")

def parse_pkgpath(pkgpath):
    """
    Function to parse a Slackware package name.
    Argument: a pathname, or just a filename
      * the filename must have at least 4 '-' separated fields
    Returns: a tuple of (catnam, prgnam, version, arch, build, tag, pkgtype)
      * the prgnam, version, arch and build fields are all required
      * None is returned if the filename doesn't have the required fields
      * if any other field is omitted, its tuple element will be a null string
    """

    pathchunks = pkgpath.split("/")
    pkgchunks = pathchunks[-1].split("-")

    if len(pkgchunks) < 4:
        return None

    prgnam = "-".join(pkgchunks[:-3])

    catnam = ""
    if len(pathchunks) >= 2:
        catnam = pathchunks[-2]
    # fixup for extra/prgnam/*.txz, {patches,testing}/packages/*.txz
    if ( catnam == prgnam or catnam == "packages" ) and len(pathchunks) >= 3:
        catnam == pathchunks[-3]

    version = pkgchunks[-3]
    arch = pkgchunks[-2]

    buildtagext = pkgchunks[-1]

    build = ""
    buildmatch = re_build.match(buildtagext)
    if buildmatch:
        build = buildmatch[0]
        buildtagext = buildtagext[buildmatch.end():]
    else:
        # if there's no matched build, everything else is probably wrong too
        return None

    ext = ""
    extmatch = re_pkgext.search(buildtagext)
    if extmatch:
        pkgtype = extmatch[0][1:4]
        buildtagext = buildtagext[0:extmatch.start()]

    tag = buildtagext

    return (catnam, prgnam, version, arch, build, tag, pkgtype)

#-------------------------------------------------------------------------------
