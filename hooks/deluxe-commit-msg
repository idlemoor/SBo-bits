#!/bin/sh

# .git/hooks/commit-msg for slackbuilds.org.
# Hubris/Laziness/Impatience Edition.

msg=$(sed "/^#/d" "$1")

if [ "$(echo "$msg" | wc -l | cut -d " " -f 1)" -ge "2" ]; then
  test "$(echo "$msg" | sed -n "2p")" = "" ||
   {
     echo "Line number 2 not empty!"
     exit 1
   }
fi

# Normalize commit messages to SBo standard.
# Three things happen here:
# 1. If only one directory had changes, and the message
#    doesn't have category/prog: as a prefix, the prefix
#    gets added.
# 2. If the commit message is "v" or "v,something", we generate
#    an 'Updated for version x.y.z.' or
#    'Updated for version x.y.z, something.' message.
# 3. If the commit message doesn't end with a period, we add one.
# None of the above happens if the commit has changes in
# more than one category/prog directory.
# If this code ever causes problems, it can be bypassed with:
# export NOAUTOCOMMIT=1

if [ "$NOAUTOCOMMIT" = "" ]; then
  # see which director(y|ies) have changes
  prefix=$(
    git status --porcelain | \
      grep '^ *M'   | \
      cut -d' ' -f3 | \
      cut -d/ -f1-2 | \
      sort -u       )
  
  # only do anything if exactly one directory had changes.
  if [ "$( echo $prefix | wc -w )" != 1 ]; then
    prefix=""
  fi
  
  if [ "$prefix" != "" ]; then
    oldmsg="$msg"
  
    case "$msg" in
      "$prefix: "*)
        ;; # do nothing if prefix already present
  
      v|v,*) # prepend '$prefix: Updated for version...'
        # handle e.g. -m'v,fixed README'
        if [ "$(echo -n "$msg" | wc -c)" != 1 ]; then
          rest=", $( echo $msg | cut -d, -f2- | sed 's,\.$,,' )"
        fi
    
        prog="$( echo $prefix | cut -d/ -f2 )"
        source $prefix/$prog.info || {
          echo "Missing $prefix/$prog.info"
          exit 1
        }
  
        msg="$prefix: Updated for version $VERSION$rest."
        ;;
  
      *) # just prepend $prefix
        msg="$prefix: $msg"
        ;;
    esac
  
    # if the message doesn't already end with a period, add one
    if [ "$( echo -n "$msg" | tail -c1 )" != "." ]; then
      msg="$msg."
    fi
  
    # tell git what the new message is
    echo "$msg" > "$1"
  
    # show the user what happened
    if [ "$msg" != "$oldmsg" ]; then
      cat <<EOF
=== Modified commit message
    was: '$oldmsg'
    now: '$msg'
If this doesn't look correct, use 'git commit --amend' to fix it.
EOF
    fi # [ "$msg" != "$oldmsg" ]
  fi   # [ "$prefix" != "" ]
fi     # [ "$NOAUTOCOMMIT" = "" ]

# test line length after any modifications made above.
test $(echo "$msg" | wc -L | cut -d " " -f 1) -lt 69 ||
 {
    echo "Line length of commit message exceeded 68 characters!"
    exit 1
 }

# add signed-off-by line.
SOB=$(git var GIT_COMMITTER_IDENT | sed -n 's/^\(.*>\).*$/Signed-off-by: \1/p')
grep -qs "^$SOB" "$1" || echo -e "\n$SOB" >> "$1"
