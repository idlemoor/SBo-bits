SlackBuilds.org related stuff

* review/ -- Review, approve and push SlackBuilds.org submissions

* parsers/ -- Python modules for parsing Slackware and SBo file formats:
  ChangeLog.txt files, SBo .info files, and Slackware package names

* hooks/ -- git hooks for SlackBuilds.org git committers
